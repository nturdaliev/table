package kg.nuriza.table;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.SharedElementCallback;
import android.view.View;
import android.view.ViewGroup;


import java.util.List;

import kg.nuriza.table.helper.Database;
import kg.nuriza.table.model.TableItem;
import kg.nuriza.table.model.adapter.TableAdapter;
import kg.nuriza.table.view.PeriodicTableView;

public class TableActivity extends Activity implements PeriodicTableView.OnItemClickListener {

    private TableAdapter mAdapter;
    private PeriodicTableView mPeriodicTableView;

    private class LoadData extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            mAdapter.destroyDrawingCache();

            mAdapter.setItems(Database.getInstance(TableActivity.this).getTableItems());

            mAdapter.buildDrawingCache(mPeriodicTableView);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            mAdapter.notifyDataSetChanged();
        }
    }

    public SharedElementCallback mSharedElementCallback = new SharedElementCallback() {

        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        public void onSharedElementStart(List<String> sharedElementNames, List<View> sharedElements,
                                         List<View> sharedElementSnapshots) {
            super.onSharedElementStart(sharedElementNames, sharedElements, sharedElementSnapshots);

            View view = sharedElements.get(0);

            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();

            view.measure(View.MeasureSpec.makeMeasureSpec(layoutParams.width,
                    View.MeasureSpec.EXACTLY), View.MeasureSpec.makeMeasureSpec(
                    layoutParams.height, View.MeasureSpec.EXACTLY));
            view.layout(view.getLeft(), view.getTop(), view.getLeft() + view.getMeasuredWidth(),
                    view.getTop() + view.getMeasuredHeight());
            view.setPivotX(0f);
            view.setPivotY(0f);
            view.setScaleX(mPeriodicTableView.getZoom());
            view.setScaleY(mPeriodicTableView.getZoom());
        }

        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        public void onSharedElementEnd(List<String> sharedElementNames, List<View> sharedElements,
                                       List<View> sharedElementSnapshots) {
            super.onSharedElementEnd(sharedElementNames, sharedElements, sharedElementSnapshots);

            View view = sharedElements.get(0);
            view.setScaleX(1f);
            view.setScaleY(1f);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.table_activity);

        mAdapter = new TableAdapter(TableActivity.this);
        mPeriodicTableView = (PeriodicTableView) findViewById(R.id.elements_table);
        mPeriodicTableView.setAdapter(mAdapter);
        mPeriodicTableView.setOnItemClickListener(this);
        mPeriodicTableView.setEmptyView(findViewById(R.id.progress_bar));
        new LoadData().execute();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mAdapter.destroyDrawingCache();
    }

    @Override
    public void onItemClick(PeriodicTableView parent, View view, int position) {
        TableItem item = mAdapter.getItem(position);

        if (item != null) {
            Intent intent = new Intent(this, ElementActivity.class);
            intent.putExtra(ElementActivity.EXTRA_ATOMIC_NUMBER, item.getNumber());

            ActivityCompat.startActivity(this, intent,
                    ActivityOptionsCompat.makeSceneTransitionAnimation(this, view,
                            getString(R.string.transition_table_item)).toBundle());
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onExitTransitionFinished() {
        View view = mPeriodicTableView.getActiveView();

        if (view != null) {
            view.setAlpha(1f);
        }
    }
}
