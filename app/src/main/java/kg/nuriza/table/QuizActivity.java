package kg.nuriza.table;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.LinkedHashSet;
import java.util.Random;

import kg.nuriza.table.helper.Database;
import kg.nuriza.table.model.ElementProperties;
import kg.nuriza.table.model.Statistics;


public class QuizActivity extends ActionBarActivity {

    public static final String QUIZ_TYPE_INDEX = "QUIZ_TYPE";
    public static final String STATISTICS_SERIALIZABLE = "STATISTICS_SERIALIZABLE";
    private static final String QUESTION_MARK = "?";
    private TextView symbolTextView, numberTextView, nameTextView, weightTextView, questionTextView, correctTextView, falseTextView, totalTextView, percentageTextView;
    private ElementProperties currentElementProperties;
    private Button checkButton;
    private int quizTypeIndex;
    private String questionTitle;
    private EditText answerEditText;
    Database database;
    Random random = new Random();
    private Statistics statistics;
    private LinkedHashSet<Integer> elementIndexes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        checkButton = (Button) findViewById(R.id.checkButton);
        questionTextView = (TextView) findViewById(R.id.text_view_question);
        numberTextView = (TextView) findViewById(R.id.element_number);
        symbolTextView = (TextView) findViewById(R.id.element_symbol);
        nameTextView = (TextView) findViewById(R.id.element_name);
        weightTextView = (TextView) findViewById(R.id.element_weight);
        answerEditText = (EditText) findViewById(R.id.edit_text_answer);
        percentageTextView = (TextView) findViewById(R.id.percentageTextView);

        /*Statistics*/
        correctTextView = (TextView) findViewById(R.id.correctTextView);
        falseTextView = (TextView) findViewById(R.id.falseTextView);
        totalTextView = (TextView) findViewById(R.id.totalTextView);


        quizTypeIndex = getIntent().getIntExtra(QUIZ_TYPE_INDEX, 0);
        setQuestionTitle();


        database = Database.getInstance(this);

        elementIndexes = new LinkedHashSet<>();

        if (savedInstanceState == null) {
            statistics = new Statistics();
        } else {
            statistics = (Statistics) savedInstanceState.getSerializable(STATISTICS_SERIALIZABLE);
            updateStatistics();
        }

        /*Set First Question*/
        setQuestion();
        checkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String answer = answerEditText.getText().toString();
                if (answer.length() > 0) {
                    if (checkAnswer(answer)) {
                        statistics.increaseNumTrue();
                    } else {
                        statistics.increaseNumFalse();
                    }
                    updateStatistics();
                    setQuestion();
                }
            }
        });
    }

    private void updateStatistics() {
        correctTextView.setText(String.valueOf(statistics.getTrueNum()));
        falseTextView.setText(String.valueOf(statistics.getFalseNum()));
        totalTextView.setText(String.valueOf(statistics.getTotal()));
        percentageTextView.setText(String.valueOf(statistics.getPercentage()));
    }

    private boolean checkAnswer(String answer) {
        answer = answer.toLowerCase();
        switch (quizTypeIndex) {
            case 0:
                return currentElementProperties.getName().toLowerCase().equals(answer);
            case 1:
                return currentElementProperties.getSymbol().toLowerCase().equals(answer);
            case 2:
                try {
                    return currentElementProperties.getNumber() == Integer.parseInt(answer);
                } catch (Exception ignore) {
                    return false;
                }
            default:
                return false;
        }
    }

    private void setQuestion() {

        int index = insertNewElementIndex(random, 117);
        currentElementProperties = database.getElementProperties(index);

        symbolTextView.setText(currentElementProperties.getSymbol());
        nameTextView.setText(currentElementProperties.getName());
        weightTextView.setText(currentElementProperties.getStandardAtomicWeight());
        numberTextView.setText(String.valueOf(currentElementProperties.getNumber()));
        answerEditText.setText("");
        switch (quizTypeIndex) {
            case 0:
                nameTextView.setText(QUESTION_MARK);
                nameTextView.setTextColor(getResources().getColor(R.color.button_learn));
                nameTextView.setEms(2);
                break;
            case 1:
                symbolTextView.setText(QUESTION_MARK);
                symbolTextView.setTextColor(getResources().getColor(R.color.button_learn));
                symbolTextView.setEms(2);
                break;
            case 2:
                numberTextView.setText(QUESTION_MARK);
                numberTextView.setTextColor(getResources().getColor(R.color.button_learn));
                numberTextView.setEms(2);
                break;
        }
    }

    private void setQuestionTitle() {
        switch (quizTypeIndex) {
            case 0:
                questionTitle = getResources().getString(R.string.name_question_title);
                break;
            case 1:
                questionTitle = getResources().getString(R.string.abbreviation_question_title);
                break;
            case 2:
                questionTitle = getResources().getString(R.string.atomic_number_question_title);
                answerEditText.setRawInputType(InputType.TYPE_NUMBER_FLAG_SIGNED | InputType.TYPE_CLASS_NUMBER);
                break;
        }
        questionTextView.setText(questionTitle);
    }

    public int insertNewElementIndex(Random random, int n) {
        int index;
        do {
            index = random.nextInt(n) + 1;
        } while (!elementIndexes.add(index));
        return index;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(STATISTICS_SERIALIZABLE, statistics);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_quiz, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (statistics.getTotal() > 0) {
            StatisticsDataSource dataSource = new StatisticsDataSource(this);
            dataSource.open();
            dataSource.createStatistics(statistics);
            dataSource.close();
        }
    }
}
