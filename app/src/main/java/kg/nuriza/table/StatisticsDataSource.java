package kg.nuriza.table;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import kg.nuriza.table.helper.DBHelper;
import kg.nuriza.table.model.Statistics;
import kg.nuriza.table.model.StatisticsTable;

public class StatisticsDataSource {

    private SQLiteDatabase database;
    private DBHelper dbHelper;
    private String[] allColumns = {
            StatisticsTable.COLUMN_ID,
            StatisticsTable.COLUMN_CORRECT_NUM,
            StatisticsTable.COLUMN_FALSE_NUM,
            StatisticsTable.COLUMN_TIMESTAMP
    };

    public StatisticsDataSource(Context context) {
        dbHelper = new DBHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void createStatistics(Statistics statistics) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(StatisticsTable.COLUMN_CORRECT_NUM, statistics.getTrueNum());
        contentValues.put(StatisticsTable.COLUMN_FALSE_NUM, statistics.getFalseNum());
        contentValues.put(StatisticsTable.COLUMN_TIMESTAMP, String.valueOf(System.currentTimeMillis()));
        database.insert(StatisticsTable.TABLE_QUIZ_RESULTS, null, contentValues);
    }

    public void deleteStatistics(long id) {
        database.delete(StatisticsTable.TABLE_QUIZ_RESULTS, StatisticsTable.COLUMN_ID
                + " = " + id, null);
    }

    public Statistics getStatistice(int id) {
        Cursor cursor = database.query(StatisticsTable.TABLE_QUIZ_RESULTS,
                allColumns, StatisticsTable.COLUMN_ID + " = " + id, null,
                null, null, null);
        cursor.moveToFirst();
        Statistics statistics = cursorToStatistics(cursor);
        cursor.close();
        return statistics;
    }

    public List<Statistics> getAllStatistics() {
        List<Statistics> statisticsList = new ArrayList<>();

        Cursor cursor = database.query(StatisticsTable.TABLE_QUIZ_RESULTS, allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Statistics statistics = cursorToStatistics(cursor);
            statisticsList.add(statistics);
            cursor.moveToNext();
        }
        return statisticsList;
    }

    public Statistics cursorToStatistics(Cursor cursor) {
        Statistics statistics = new Statistics();
        statistics.setId(cursor.getInt(0));
        statistics.setTrueNum(cursor.getInt(1));
        statistics.setFalseNum(cursor.getInt(2));
        statistics.setTimestamp(cursor.getString(3));
        return statistics;
    }
}
