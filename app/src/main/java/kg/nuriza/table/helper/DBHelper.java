package kg.nuriza.table.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import kg.nuriza.table.model.NotesTable;
import kg.nuriza.table.model.StatisticsTable;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "periodic_table.db";
    public static final int DATABASE_VERSION = 2;


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        StatisticsTable.onCreate(db);
        NotesTable.onCreate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        StatisticsTable.onUpgrade(db);
        NotesTable.onUpgrade(db);
    }
}
