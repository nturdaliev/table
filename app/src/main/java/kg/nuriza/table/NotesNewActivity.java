package kg.nuriza.table;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class NotesNewActivity extends ActionBarActivity {

    private NotesDataSource dataSource;
    private EditText editTextDescription;
    private EditText editTextTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes_new);

        editTextTitle = (EditText) findViewById(R.id.editTextTitle);

        editTextDescription = (EditText) findViewById(R.id.editTextDescription);

        Button button = (Button) findViewById(R.id.buttonCreate);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNoteValid()) {
                    Note note = new Note(editTextTitle.getText().toString(), editTextDescription.getText().toString());
                    dataSource = new NotesDataSource(NotesNewActivity.this);
                    dataSource.open();
                    dataSource.createNote(note);
                    dataSource.close();
                    Intent intent = new Intent(NotesNewActivity.this, NoteActivity.class);
                    intent.putExtra(Note.TAG, note);
                    startActivity(intent);
                } else {
                    Toast.makeText(NotesNewActivity.this, R.string.note_not_valid, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private boolean isNoteValid() {
        return (editTextTitle.getText().toString().trim().length() > 0 && editTextDescription.getText().toString().trim().length() > 0);
    }
}
