package kg.nuriza.table;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

import kg.nuriza.table.model.Statistics;
import kg.nuriza.table.model.adapter.StatisticsAdapter;


public class StatisticsActivity extends ActionBarActivity {

    private StatisticsDataSource datasource;
    List<Statistics> statisticsList;
    ArrayAdapter<Statistics> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        final ListView listView = (ListView) findViewById(R.id.listViewStatistics);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(StatisticsActivity.this);
                builder.setMessage("Delete quiz result");
                builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int currentId = (Integer) view.getTag();
                        datasource.deleteStatistics(currentId);
                        datasource.open();

                        statisticsList = datasource.getAllStatistics();

                        adapter = new StatisticsAdapter(StatisticsActivity.this, statisticsList);
                        listView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                builder.show();
                return false;
            }
        });

        datasource = new StatisticsDataSource(this);
        datasource.open();

        statisticsList = datasource.getAllStatistics();

        adapter = new StatisticsAdapter(this, statisticsList);
        listView.setAdapter(adapter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        datasource.close();
    }
}
