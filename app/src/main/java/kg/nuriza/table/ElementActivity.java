package kg.nuriza.table;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import kg.nuriza.table.helper.Database;
import kg.nuriza.table.model.ElementProperties;
import kg.nuriza.table.model.adapter.PropertiesAdapter;


public class ElementActivity extends ActionBarActivity implements ListView.OnItemClickListener {


    public static final String EXTRA_ATOMIC_NUMBER = "kg.nuriza.table.AtomicNumber";

    private ListView listView;
    private ElementProperties elementProperties;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_element);

        elementProperties = Database.getInstance(this).getElementProperties(
                getIntent().getIntExtra(EXTRA_ATOMIC_NUMBER, 1));

        listView = (ListView) findViewById(R.id.element_properties_list);
        PropertiesAdapter adapter = new PropertiesAdapter(ElementActivity.this, elementProperties);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
        listView.setOnHierarchyChangeListener(new ViewGroup.OnHierarchyChangeListener() {
            @Override
            public void onChildViewAdded(View parent, View child) {
                if (child.findViewById(R.id.tile_view) != null) {
                    ElementActivity.this.supportStartPostponedEnterTransition();

                    listView.setOnHierarchyChangeListener(null);
                }
            }

            @Override
            public void onChildViewRemoved(View parent, View child) {
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_element, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(elementProperties.getWikipediaLink()));
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
}
