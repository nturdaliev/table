package kg.nuriza.table;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;

import kg.nuriza.table.helper.Database;
import kg.nuriza.table.model.ElementListItem;
import kg.nuriza.table.model.adapter.ElementsAdapter;


public class IndexActivity extends ActionBarActivity implements SearchView.OnQueryTextListener, ListView.OnItemClickListener {

    private ElementsAdapter elementsAdapter;
    private ListView listView;
    private View emptyView;
    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);

        elementsAdapter = new ElementsAdapter(IndexActivity.this);
        emptyView = findViewById(R.id.empty_elements_list);
        listView = (ListView) findViewById(R.id.elements_list);
        listView.setAdapter(elementsAdapter);
        listView.setEmptyView(!elementsAdapter.isEmpty() ? emptyView :
                findViewById(R.id.progress_bar));
        listView.setOnItemClickListener(this);
        //listView.setTextFilterEnabled(true);
        new LoadData().execute();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_index, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(this);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            searchView.clearFocus();
            elementsAdapter.clearFilter();
        } else {
            elementsAdapter.filter(newText);
        }

        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(IndexActivity.this, ElementActivity.class);
        intent.putExtra(ElementActivity.EXTRA_ATOMIC_NUMBER, elementsAdapter.getItem(position).getNumber());
        startActivity(intent);
    }

    public class LoadData extends AsyncTask<Void, Void, ElementListItem[]> {

        @Override
        protected ElementListItem[] doInBackground(Void... params) {
            return Database.getInstance(IndexActivity.this).getElementListItems();
        }

        @Override
        protected void onPostExecute(ElementListItem[] result) {
            elementsAdapter.setItems(result);

            elementsAdapter.notifyDataSetChanged();

            listView.setEmptyView(emptyView);
        }
    }
}
