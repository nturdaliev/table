package kg.nuriza.table.model.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import kg.nuriza.table.R;
import kg.nuriza.table.model.Statistics;


public class StatisticsAdapter extends ArrayAdapter<Statistics> {

    private List<Statistics> statisticses;
    private Context context;

    public StatisticsAdapter(Context context, List<Statistics> statisticses) {
        super(context, R.layout.statistics_item, statisticses);
        this.statisticses = statisticses;
        this.context = context;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.statistics_item, parent, false);
        TextView textViewTimestamp = (TextView) rowView.findViewById(R.id.textViewTimestamp);
        TextView textViewCorrect = (TextView) rowView.findViewById(R.id.textViewTrue);
        TextView textViewFalse = (TextView) rowView.findViewById(R.id.textViewWrong);
        TextView textViewTotal = (TextView) rowView.findViewById(R.id.textViewTotal);
        TextView textViewPercentage = (TextView) rowView.findViewById(R.id.textViewPercentage);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yy \n HH:mm:ss");

        textViewTimestamp.setText(simpleDateFormat.format(new Date(Long.parseLong(statisticses.get(position).getTimestamp()))));
        textViewCorrect.setText(String.valueOf(statisticses.get(position).getTrueNum()));
        textViewFalse.setText(String.valueOf(statisticses.get(position).getFalseNum()));
        textViewTotal.setText(String.valueOf(statisticses.get(position).getTotal()));
        textViewPercentage.setText(String.valueOf(String.valueOf(statisticses.get(position).getPercentage())));
        rowView.setTag(Integer.valueOf(statisticses.get(position).getId()));
        return rowView;
    }
}
