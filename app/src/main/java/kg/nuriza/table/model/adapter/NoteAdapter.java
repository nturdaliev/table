package kg.nuriza.table.model.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import kg.nuriza.table.Note;
import kg.nuriza.table.R;


public class NoteAdapter extends ArrayAdapter<Note> {

    private List<Note> notes;
    private Context context;

    public NoteAdapter(Context context, List<Note> notes) {
        super(context, R.layout.note_item, notes);
        this.notes = notes;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.note_item, parent, false);
        TextView textViewTitle = (TextView) rowView.findViewById(R.id.note_title);
        textViewTitle.setText(notes.get(position).getTitle());
        rowView.setTag(Integer.valueOf(notes.get(position).getId()));
        return rowView;
    }
}
