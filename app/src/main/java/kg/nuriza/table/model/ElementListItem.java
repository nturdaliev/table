package kg.nuriza.table.model;

public class ElementListItem {

    private String name, symbol;
    private int number;

    public String getName() {
        return name;
    }

    public String getSymbol() {
        return symbol;
    }

    public int getNumber() {
        return number;
    }
}
