package kg.nuriza.table.model;

import android.database.sqlite.SQLiteDatabase;

public class StatisticsTable {

    public static final String TABLE_QUIZ_RESULTS = "quiz_results";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_CORRECT_NUM = "correct_num";
    public static final String COLUMN_FALSE_NUM = "false_num";
    public static final String COLUMN_TIMESTAMP = "time_stamp";

    private static final String DATABASE_CREATE = "create table " +
            TABLE_QUIZ_RESULTS
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_CORRECT_NUM + " integer not null, "
            + COLUMN_FALSE_NUM + " integer not null, "
            + COLUMN_TIMESTAMP + " text not null"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_QUIZ_RESULTS);
        onCreate(database);
    }
}
