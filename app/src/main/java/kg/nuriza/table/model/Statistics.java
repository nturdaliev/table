package kg.nuriza.table.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class Statistics implements Serializable {

    private int id;
    private int falseNum;
    private int trueNum;
    private String timestamp;

    public Statistics() {
        falseNum = 0;
        trueNum = 0;
    }


    public int getFalseNum() {
        return falseNum;
    }

    public void setFalseNum(int falseNum) {
        this.falseNum = falseNum;
    }

    public int getTrueNum() {
        return trueNum;
    }

    public void setTrueNum(int numTrue) {
        this.trueNum = numTrue;
    }

    public void increaseNumTrue() {
        trueNum++;
    }

    public void increaseNumFalse() {
        falseNum++;
    }

    public int getTotal() {
        return falseNum + trueNum;
    }

    public double getPercentage() {
        BigDecimal bigDecimal = new BigDecimal(((double) trueNum / (double) (getTotal())) * 100).setScale(2, RoundingMode.CEILING);
        return bigDecimal.doubleValue();
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return String.valueOf(getTotal()) + getTimestamp();
    }
}
