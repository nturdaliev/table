package kg.nuriza.table;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import kg.nuriza.table.helper.DBHelper;
import kg.nuriza.table.model.NotesTable;
import kg.nuriza.table.Note;

public class NotesDataSource {

    private SQLiteDatabase database;
    private DBHelper dbHelper;
    private String[] allColumns = {
            NotesTable.COLUMN_ID,
            NotesTable.COLUMN_TITLE,
            NotesTable.COLUMN_DESCRIPTION
    };

    public NotesDataSource(Context context) {
        dbHelper = new DBHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void createNote(Note note) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(NotesTable.COLUMN_TITLE, note.getTitle());
        contentValues.put(NotesTable.COLUMN_DESCRIPTION, note.getDescription());
        database.insert(NotesTable.TABLE_NOTES, null, contentValues);
    }

    public void deleteNote(long id) {
        database.delete(NotesTable.TABLE_NOTES, NotesTable.COLUMN_ID
                + " = " + id, null);
    }

    public Note getNote(int id) {
        Cursor cursor = database.query(NotesTable.TABLE_NOTES,
                allColumns, NotesTable.COLUMN_ID + " = " + id, null,
                null, null, null);
        cursor.moveToFirst();
        Note note = cursorToNote(cursor);
        cursor.close();
        return note;
    }

    public List<Note> getAllNotes() {
        List<Note> notesList = new ArrayList<>();

        Cursor cursor = database.query(NotesTable.TABLE_NOTES, allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Note notes = cursorToNote(cursor);
            notesList.add(notes);
            cursor.moveToNext();
        }
        return notesList;
    }

    public Note cursorToNote(Cursor cursor) {
        Note note = new Note();
        note.setId(cursor.getInt(0));
        note.setTitle(cursor.getString(1));
        note.setDescription(cursor.getString(2));
        return note;
    }
}
