package kg.nuriza.table;

import java.io.Serializable;

public class Note implements Serializable {
    private String title;
    private String description;
    private int id;

    public static final String TAG = "note";

    public Note() {
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public Note(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
