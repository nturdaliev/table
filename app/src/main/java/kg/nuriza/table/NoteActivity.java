package kg.nuriza.table;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class NoteActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        Intent intent = getIntent();
        Note note = (Note) intent.getSerializableExtra(Note.TAG);

        TextView textViewTitle = (TextView) findViewById(R.id.textViewTitle);
        TextView textViewDescription = (TextView) findViewById(R.id.textViewDescription);

        textViewTitle.setText(note.getTitle());
        textViewDescription.setText(note.getDescription());

    }
}
